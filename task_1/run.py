import sys

summary = 0
items = []
is_first = True
for line in sys.stdin.readlines():
    if not is_first:
        item = float(line)
        summary += item
        items.append(item)
    else:
        is_first = False

print("\n".join([f'{value / summary:.3f}' for value in items]))
