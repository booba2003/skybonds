import sys

days = 0
lots = 0
cash = 0

end_in_days = 30

items = []
is_first = True
for data in sys.stdin.readlines():
    line = data.strip()
    if line:
        if not is_first:
            line_data = line.split(' ')
            price_relative = float(line_data[2])
            price_absolute = price_relative * 10
            amount = int(line_data[3])
            bond_day = int(line_data[0])
            items.append({
                'day': bond_day,
                'title': line_data[1],
                'price_src': line_data[2],
                'amount': amount,
                'price_total': price_absolute * amount,

                # вычисляем потенциальную выгоду
                'potential_income': amount * (end_in_days - bond_day + days) - ((price_absolute - 1000) * amount)
            })
        else:
            trader_data = line.split(' ')
            days = int(trader_data[0])
            lots = int(trader_data[1])
            cash = float(trader_data[2])
            is_first = False

# приоритезируем элементы: самые выгодные покупки сверху
prioritized_items = sorted(items, key=lambda x: x.get('potential_income'), reverse=True)

recommendations = []
bonds_bought = set()
income = 0

# обходим всех прориоритезированные элементы, и покупаем те, которые можем себе позволить
for item in prioritized_items:
    if item.get('title') not in bonds_bought and item.get('price_total') <= cash:
        bonds_bought.add(item.get('title'))
        recommendations.append(item)
        cash -= item.get('price_total')
        income += item.get('potential_income')

print(income)
result = [
    f'{item.get("day")} {item.get("title")} {item.get("price_src")} {item.get("amount")}'
    for item in recommendations
]
print("\n".join(result))
print("")

